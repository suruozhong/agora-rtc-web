@echo off
chcp 65001

rem 服务器IP
set ip=120.24.202.127
rem 服务器密码
set password=zhong@123
rem linux服务器路径
set linuxDir=/home/web/agora/

rem 拷贝文件完整路径
set folder=dist\

echo "开始打包"
cd ..
call npm run build

echo "传输到linux服务器"
pscp -pw %password% -r %folder% root@%ip%:%linuxDir%

@cmd.exe
exist